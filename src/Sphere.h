#ifndef SPHERE_H
#define SPHERE_H
#include "Classes.h"
#include "SceneObject.h"

class Sphere : SceneObject
{
public:
	Sphere(GLdouble radius, GLint slices, GLint stacks);
	void render();
	~Sphere();
private:
	GLdouble radius;
	GLint slices, stacks;
};
#endif
