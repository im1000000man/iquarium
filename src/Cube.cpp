#include "Cube.h"

Cube::Cube(GLfloat size) :SceneObject()
{
	this->size = size;
}

void Cube::render()
{
	glColor4f(r, g, b, a);
	//alphaCube(size);
	glutSolidCube(size);
}

Cube::~Cube()
{
	deleteVectors();
}

void Cube::alphaCube(GLfloat size){
	GLfloat half_size = size / 2;
	//ClockWise winding
	glBegin(GL_QUAD_STRIP);
	//Far frontal side |_|
	glVertex3d(-half_size,-half_size,half_size);
	glVertex3d(-half_size,half_size,half_size);
	glVertex3d(half_size,half_size,half_size);
	glVertex3d(half_size,-half_size,half_size);
	glEnd();
}
