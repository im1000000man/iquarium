#ifndef TEAPOT_H
#define TEAPOT_H
#include "Classes.h"
#include "SceneObject.h"

class Teapot : SceneObject
{
    public:
       Teapot(GLfloat size);
       void render();
	~Teapot(); 
    private:
       GLfloat size;
};
#endif
