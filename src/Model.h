#ifndef MODEL_H
#define MODEL_H
#include "Classes.h"
#include "SceneObject.h"
#include "glm.h"
#include <stdio.h>
#include "Scene.h"

using namespace std;

class Model : SceneObject
{
public:
	GLMmodel *model;
	GLuint displayList;
	Model(string modelName);
	~Model();
	void render();
	void action(Scene* scene, string modelName);
};

#endif
