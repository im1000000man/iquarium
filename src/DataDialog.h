#pragma once
#include "Classes.h"
#include "SceneObject.h"

class DataDialog : SceneObject
{
public:
    DataDialog(GLfloat width,GLfloat height,GLfloat alpha);
    DataDialog(GLfloat width,GLfloat height,GLfloat alpha,GLenum wrapping);
    ~DataDialog();
    void render();
    
private:
    GLfloat width;
    GLfloat height;
    GLfloat alpha;
};

