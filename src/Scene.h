#ifndef SCENE_H
#define SCENE_H
#include "Classes.h"
#include "SceneObject.h"
#include "Camera.h"
#include "LightManager.h"
#include "TextureManager.h"

using namespace std;

class Scene {

private:
	int windowWidth;
	int windowHeight;

	//FishTank CoordinateLimits

	unsigned long currentFrame;
	GLint frameRate, frameTime, currentTime;
	Camera* currentCamera;
	string currentCameraString;
	LightManager* lightManager;
	TextureManager *textureManager;
	GLuint selectBuf[PICKBUFSIZE];
	GLuint lastHitname;
	Vec* limits;
	void collisions(string objectName, SceneObject *o);
	bool collision(GLfloat model_coordinate, GLfloat limit_coordinate);

public:
	map<string, SceneObject *> objects;
	map<string, Camera*> cameras;
	void begin(int x, int y, int width, int height, string title, GLfloat r,
			GLfloat g, GLfloat b);
	//void lighting();
	void render();
	void resize();
	void resize(int currentWidth, int currentHeight);
	void keyboard(unsigned char key, int x, int y);
	void specialKeyboard(int key, int x, int y);
	void end();
	void add(SceneObject *newObject, string name);
	void addCamera(string, bool, GLfloat, GLfloat, GLfloat);
	void setCurrentCamera(string);
	Camera* getCurrentCamera();
	void setFrameRate(int);
	void idle();
	Scene();
	LightManager *getLightManager();
	void setObjectTexture(string objectName, string textureName);
	TextureManager *getTextureManager();
	void mouseClick(int button, int state, int x, int y);
	void startPicking(int x, int y);
	void stopPicking();
	void processHits(GLint hits);
	void actions(string objectName);
	//Scene Fish Tank Coordinates Limits
	void setLimits(const GLfloat x, const GLfloat y, const GLfloat z);
	Vec* getLimits();
};

#endif
