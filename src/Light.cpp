/*
 * Light.cpp
 *
 *  Created on: Mar 28, 2015
 */
#include "Light.h"
#include <stdio.h>

//Light.cpp
Light::Light() {
	source = DIRECTIONAL;
	enabled = false;
	shininess = 0.0;
	cutOff = 0.0;
	attenuation = 0.0;
	for (int i = 0; i < 4; i++) {
		lightPosition[i] = 0;
		ambient[i] = 0;
		diffuse[i] = 0;
		specular[i] = 0;
		if (i < 3) {
			direction[i] = 0;
		}
	}
}
Light::~Light() {
	string sourceNames[] = { "directional", "point", "spot" };
	printf("~Light():");
	if (enabled)
		printf("enabled %s\n", sourceNames[source].c_str());
	else
		printf("disabled\n");
}
void Light::setEnabled(bool enabled) {
	this->enabled = enabled;
}
void Light::setPosition(GLfloat x, GLfloat y, GLfloat z) {
	lightPosition[0] = x;
	lightPosition[1] = y;
	lightPosition[2] = z;
	if (source == DIRECTIONAL)
		lightPosition[3] = 0.0;
	else
		lightPosition[3] = 1.0;
}
void Light::setAmbient(GLfloat r, GLfloat g, GLfloat b) {
	ambient[0] = r;
	ambient[1] = g;
	ambient[2] = b;
	ambient[3] = 1.0;
}
void Light::setDiffuse(GLfloat r, GLfloat g, GLfloat b) {
	diffuse[0] = r;
	diffuse[1] = g;
	diffuse[2] = b;
	diffuse[3] = 1.0;
}
void Light::setSpecular(GLfloat r, GLfloat g, GLfloat b, GLfloat shininess) {
	specular[0] = r;
	specular[1] = g;
	specular[2] = b;
	specular[3] = 1.0;
	this->shininess = shininess;
}
void Light::setSpot(GLfloat x, GLfloat y, GLfloat z, GLfloat xd, GLfloat yd,
		GLfloat zd, GLfloat cutOff, GLfloat attenuation) {
	source = SPOT;
	lightPosition[0] = x;
	lightPosition[1] = y;
	lightPosition[2] = z;
	lightPosition[3] = 1.0;
	direction[0] = xd;
	direction[1] = yd;
	direction[2] = zd;
	enabled = true;
	this->cutOff = cutOff;
	this->attenuation = attenuation;
}
void Light::setPoint(GLfloat x, GLfloat y, GLfloat z, GLfloat attenuation) {
	source = POINT;
	lightPosition[0] = x;
	lightPosition[1] = y;
	lightPosition[2] = z;
	lightPosition[3] = 1.0;
	enabled = true;
	this->attenuation = attenuation;
}
void Light::setDirectional(GLfloat x, GLfloat y, GLfloat z) {
	source = DIRECTIONAL;
	lightPosition[0] = x;
	lightPosition[1] = y;
	lightPosition[2] = z;
	lightPosition[3] = 0.0;
	enabled = true;
	attenuation = 0.0;
}

bool Light::isEnabled(){
	return this->enabled;
}

LightSource Light::getSource(){
	return this->source;
}

GLfloat* Light::getLightPosition(){
	return this->lightPosition;
}

GLfloat* Light::getAmbient(){
	return this->ambient;
}
GLfloat* Light::getDiffuse(){
	return this->diffuse;
}
GLfloat* Light::getSpecular(){
	return this->specular;
}
GLfloat* Light::getDirection(){
	return this->direction;
}
GLfloat Light::getShininess(){
	return this->shininess;
}
GLfloat Light::getCutOff(){
	return this->cutOff;
}
GLfloat Light::getAttenuation(){
	return this->attenuation;
}

