#include "Camera.h"
#include "stdio.h"

Camera::Camera(bool perspective, GLfloat fovy, GLfloat nearPlane, GLfloat farPlane) {
	this->perspective = perspective;
	this->fovy = fovy;
	this->nearPlane = nearPlane;
	this->farPlane = farPlane;
}

void setCameraVectorValues(Vec* vector, const GLfloat x, const GLfloat y, const GLfloat z){
	vector->x = x;
	vector->y = y;
	vector->z = z;
}

void Camera::setPosition(GLfloat x, GLfloat y, GLfloat z){
	setCameraVectorValues(this->position, x, y, z);
}
void Camera::setSpeed(GLfloat x, GLfloat y, GLfloat z){
	setCameraVectorValues(this->speed, x, y, z);	
}
void Camera::setRotation(GLfloat x, GLfloat y, GLfloat z){
	setCameraVectorValues(this->rotation, x, y, z);
}
void Camera::setRotationSpeed(GLfloat x, GLfloat y, GLfloat z){
	setCameraVectorValues(this->rotationSpeed, x, y, z);
}
bool Camera::getPerspective() {
	return this->perspective;
}
GLfloat Camera::getFovy(){
	return this->fovy;
}
GLfloat Camera::getNear(){
	return this->nearPlane;
}
GLfloat Camera::getFar(){
	return this->farPlane;
}
GLfloat Camera::getLeft(){
	return this->fovy*-1;
}
GLfloat Camera::getRight(){
	return this->fovy;
}
GLfloat Camera::getTop(){
	return this->fovy;
}
GLfloat Camera::getBottom(){
	return this->fovy*-1;
}
Vec* Camera::getPosition(){
	return this->position;
}
Vec* Camera::getRotation(){
	return this->rotation;
}

Camera::~Camera()
{
	deleteVectors();
}

void Camera::setForwardSpeed()
{
    float angle=DEG_TO_RAD*rotation->y;
    float x=sin(angle)*DELTA_TRAN;
    float z=cos(angle)*DELTA_TRAN;
    setSpeed(x,0,z);
}

void Camera::setBackwardSpeed()
{
    float angle=DEG_TO_RAD*rotation->y;
    float x=-sin(angle)*DELTA_TRAN;
    float z=-cos(angle)*DELTA_TRAN;
    setSpeed(x,0,z);
}
