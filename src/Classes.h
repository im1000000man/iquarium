#ifndef CLASSES_H
#define CLASSES_H
//Standard libraries includes
#include <map>
#include <ctime>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
// For Windows x32 and x64:
#if defined(_WIN32)
#include <gl/glut.h>
#include <math.h>

// For Mac OSX:
#elif defined(__APPLE__) && defined(__MACH__)
#include <gl/glut.h>
#include <math.h>

// For Linux:
#else
#include <GL/freeglut.h>
#include <GL/gl.h>


#endif

//Add every new class in the project, in alphabetical order
class camera;
class cube;
class Scene;
class Sphere;
class SceneObject;
class Teapot;
class Vec;

#define ESC 27

#define RFACTOR 1000
#define rval(min,max) min+(max-min)*(rand() % RFACTOR)/RFACTOR
#define MAXFILENAME 50
#define DELTA_TRAN 0.2
#define DELTA_ROT  3.0
#define DEG_TO_RAD 0.0174532925

//Lighting
#define MAX_LIGHTS 8
#define MARKER_RADIUS 0.15
#define MARKER_SECTIONS 7
#define SPECULAR_SHININESS 64.0
enum LightSource {DIRECTIONAL,POINT,SPOT};

//Texturing
#define INVALID_TEXTURE -1
#define DEFAULT_MODE 0
//Picking
#define PICKBUFSIZE 512

#endif
