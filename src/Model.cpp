#include "Model.h"

Model::Model(string modelName) :
		SceneObject() {
	char fileName[MAXFILENAME];
	sprintf(fileName, "models/%s.obj", modelName.c_str());
	displayList = 0;
	model = glmReadOBJ(fileName);
	if (model) {
		glmUnitize(model);
		glmFacetNormals(model);
		glmVertexNormals(model, 90.0);
	}
}

void Model::render() {
	GLuint flags = GLM_SMOOTH | GLM_MATERIAL;
	if (textureId != INVALID_TEXTURE) {
		flags = GLM_SMOOTH | GLM_TEXTURE;
	}
	glPushAttrib(GL_COLOR_MATERIAL);
	// First time, create display list
	if (!displayList) {
		displayList = glmList(model, flags);
	}
	// If displayList is available,draws from it,else draws from model
	if (displayList) {
		glCallList(displayList);
	} else {
		glmDraw(model, flags);
	}
	glPopAttrib();
}

Model::~Model() {
	//printf("~Model()");
	if (model) {
		glmDelete(model);
	}
	deleteVectors();
}

void Model::action(Scene* scene, string objectName) {
	//TODO: Receive a func void* foo (void*) for a custom action of each instance.
	//Examples: qsort for a function as parameter, posix thread for void* foo (void*) usage.

	cout << "user clicked on: " << objectName << endl;
// "ballena", "barracuda", "cirujanoamarillo", "dori", "mariposa", "nigris", "pespada", "trompeta"
	if (strncmp(objectName.c_str(), "fish_", 5) == 0) {
		if(objectName.find("angel", 0) != -1)
			scene->setObjectTexture("details_tv", "Textura_TeleAngel");
		if(objectName.find("ballena", 0) != -1)
					scene->setObjectTexture("details_tv", "Textura_TeleBallena");
		if(objectName.find("barracuda", 0) != -1)
					scene->setObjectTexture("details_tv", "Textura_TeleBarracuda");
		if(objectName.find("cirujanoamarillo", 0) != -1)
					scene->setObjectTexture("details_tv", "Textura_TeleCirujanoAmarillo");
		if(objectName.find("dori", 0) != -1)
					scene->setObjectTexture("details_tv", "Textura_TeleCirujanoAzul");
		if(objectName.find("mariposa", 0) != -1)
					scene->setObjectTexture("details_tv", "Textura_TeleMariposa");
		if(objectName.find("nigris", 0) != -1)
					scene->setObjectTexture("details_tv", "Textura_TeleCirujanoNegro");
		if(objectName.find("pespada", 0) != -1)
					scene->setObjectTexture("details_tv", "Textura_TeleEspada");
		if(objectName.find("trompeta", 0) != -1)
							scene->setObjectTexture("details_tv", "Textura_TeleTrompeta");
			scene->setCurrentCamera("details");
			scene->resize();
	}
	cout << "Object " << objectName << " action dispatched" << endl;
}


