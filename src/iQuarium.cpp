#include "Classes.h"
#include "Scene.h"
#include "Teapot.h"
#include "Cube.h"
#include "Sphere.h"
#include "Camera.h"
#include "Model.h"
#include "Quad.h"
#include "DataDialog.h"
#include <iostream>
#include <sstream>
#include <cmath>

Scene *scene = new Scene();

 #define FISHES_SIZE 9
 const string FISHES[] = { "angel", "ballena", "barracuda", "cirujanoamarillo", "dori", "mariposa", "nigris", "pespada", "trompeta" };

void render() {
	scene->render();
}

void resize(int width, int height) {
	scene->resize(width, height);
}

void keyboard(unsigned char key, int x, int y) {
	scene->keyboard(key, x, y);
}

void specialKeyboard(int key, int x, int y) {
	scene->specialKeyboard(key, x, y);
}

void mouseClick(int button, int state, int x, int y) {
	scene->mouseClick(button, state, x, y);
}

void createCube() {
	scene->add((SceneObject *) new Cube(1), "cube");
	scene->objects["cube"]->setColor4(0, 0, 1, .1);
	scene->objects["cube"]->setPosition(0, 0, 0);
	//scene->objects["cube"]->setScale(1, 1, 1);
	//scene->objects["cube"]->setRotation(10, 10, 10);
}

double rdouble() {
	return (((double) rand()) / RAND_MAX);
}
void createFishes(int limit, int offset = 0) {

	for (int i = 0; i < limit; i++) {
		int randPos = rand() % FISHES_SIZE;
		string fish = FISHES[randPos];

		stringstream s;
		s << "fish_" << fish << "_" << (i + offset);
		string fishName = s.str();

		scene->add((SceneObject *) new Model(fish), fishName);
		scene->objects[fishName]->setRandomColor();
		scene->objects[fishName]->setRandomPosition(-8, 8);
		scene->objects[fishName]->setRandomScale(.7, .7);
		scene->objects[fishName]->setRandomSpeed(-.01, .01);
		scene->objects[fishName]->setRandomRotation(-50, 50);
		scene->setObjectTexture(fishName, FISHES[randPos]);
		//scene->setObjectTexture(fishName, FISHES_TEXTURES[randPos]);
	}
	cout << "Created " << limit << " fishes." << endl;
}

void createFishTankRoom(int roomSize) {
	//Room coordinates
	int xCoord = 0, yCoord = 15, zCoord = -7.5;

	//Piso
	scene->add((SceneObject *) new Model("Piso"), "floor");
	scene->setObjectTexture("floor", "Textura_Piso");
	scene->objects["floor"]->setPosition(0, -roomSize/2 + 6, roomSize/2);
	scene->objects["floor"]->setScale(roomSize, roomSize, roomSize);

	//Pared cuarto b
	scene->add((SceneObject *) new Model("Pared"), "pared_b");
	scene->setObjectTexture("pared_b", "Textura_Pared");
	scene->objects["pared_b"]->setPosition(xCoord, yCoord, zCoord);
	scene->objects["pared_b"]->setScale(roomSize, roomSize, roomSize);

	//Pared cuarto l
	scene->add((SceneObject *) new Model("Pared"), "pared_l");
	scene->setObjectTexture("pared_l", "Textura_Pared");
	scene->objects["pared_l"]->setPosition(-roomSize + xCoord, yCoord, zCoord);
	scene->objects["pared_l"]->setScale(roomSize, roomSize, roomSize * 1.6);
	scene->objects["pared_l"]->setRotation(0, 90, 0);

	//Pared cuarto r
	scene->add((SceneObject *) new Model("Pared"), "pared_r");
	scene->setObjectTexture("pared_r", "Textura_Pared");
	scene->objects["pared_r"]->setPosition(roomSize + xCoord, yCoord, zCoord);
	scene->objects["pared_r"]->setScale(roomSize, roomSize, roomSize* 1.6);
	scene->objects["pared_r"]->setRotation(0, -90, 0);

	scene->getLimits()->setValues(38, 9, 8);

	//Mesa
	scene->add((SceneObject *) new Model("MesaPescera"), "FishTank_Table");
	scene->setObjectTexture("FishTank_Table", "TexturaMesaPescera");
	scene->objects["FishTank_Table"]->setPosition(0, -17, 0);
	scene->objects["FishTank_Table"]->setScale(40, 20, 30);

	// Arena
	scene->add((SceneObject *) new Model("Arena"), "arena_pecera");
	scene->setObjectTexture("arena_pecera", "Textura_Arena");
	scene->objects["arena_pecera"]->setPosition(0, -14, 3);
	scene->objects["arena_pecera"]->setScale(40, 20, 10);

	//Fish tank metal cover
	scene->add((SceneObject *) new Model("Pescera2"), "pecera");
	scene->setObjectTexture("pecera", "TexturaPescera2");
	scene->objects["pecera"]->setPosition(0, -2, 3);
	scene->objects["pecera"]->setScale(40, 40, 10);

	//Fish tank windows
	//Front

	scene->add((SceneObject *) new Quad(80, 24, 0.5), "window_f");
	scene->setObjectTexture("window_f", "water");
	scene->objects["window_f"]->setTextGenMode(GL_SPHERE_MAP);
	scene->objects["window_f"]->setPosition(0, -2, 13);
	scene->objects["window_f"]->setVisible(true);

	//Back
	scene->add((SceneObject *) new Quad(80, 24, 0.5), "window_b");
	scene->setObjectTexture("window_b", "water");
	scene->objects["window_b"]->setTextGenMode(GL_SPHERE_MAP);
	scene->objects["window_b"]->setPosition(0, -9, -8);
	scene->objects["window_b"]->setVisible(true);

	int x, y= -2.5, z = -2;
	int sx, sy, sz ;
	//Rside
	scene->add((SceneObject *) new Quad(30, 25, 0.5), "window_r");
	scene->setObjectTexture("window_r", "water");
	scene->objects["window_r"]->setTextGenMode(GL_SPHERE_MAP);
	scene->objects["window_r"]->setPosition(40, y, z);
	scene->objects["window_r"]->setRotation(0, 90, 0);
	scene->objects["window_r"]->setVisible(true);
	//RsideInverted
	scene->add((SceneObject *) new Quad(30, 25, 0.5), "window_r1");
	scene->setObjectTexture("window_r1", "water");
	scene->objects["window_r1"]->setTextGenMode(GL_SPHERE_MAP);
	scene->objects["window_r1"]->setPosition(40, y, z);
	scene->objects["window_r1"]->setRotation(0, -90, 0);
	scene->objects["window_r1"]->setVisible(true);

	//Lside
	scene->add((SceneObject *) new Quad(30, 25, 0.5), "window_l");
	scene->setObjectTexture("window_l", "water");
	scene->objects["window_l"]->setTextGenMode(GL_SPHERE_MAP);
	scene->objects["window_l"]->setPosition(-40, y, z);
	scene->objects["window_l"]->setRotation(0, 90, 0);
	scene->objects["window_l"]->setVisible(true);
}

void loadModels() {

//Main Scene: Fish tank room
	createFishTankRoom(50);

	//Main TV
	scene->add((SceneObject *) new Model("Tele"), "details_tv");
	scene->setObjectTexture("details_tv", "Textura_Tele");
	scene->objects["details_tv"]->setScale(35, 35, 35);
	scene->objects["details_tv"]->setPosition(0, 30, -5);

	// Diver
	scene->add((SceneObject *) new Model("Buzo3"), "diver");
	scene->setObjectTexture("diver", "pSphere6Shape_color");
	scene->objects["diver"]->setScale(25, 25, 25);
	scene->objects["diver"]->setPosition(0, 0, 0);
	scene->objects["diver"]->setRotation(0, 180, 0);

	// Tank house plant
	scene->add((SceneObject *) new Model("coral1"), "coral_1");
	scene->setObjectTexture("coral_1", "coral1");
	scene->objects["coral_1"]->setPosition(-5, -13, 0);

	// Tank Coral 2
	scene->add((SceneObject *) new Model("coral2"), "coral_2");
	scene->objects["coral_2"]->setScale(3, 3, 3);
	scene->setObjectTexture("coral_2", "coral2");
	scene->objects["coral_2"]->setPosition(3, -13, 0);
}

void createScene() {
	createFishes(30);
	loadModels();
}

void createCameras() {

	int xCoord = 0, yCoord = 0, zCoord = -40;
	//Assigned to key '1'
	//First person Camera "Googles view"
	scene->addCamera("first-person", true, 45, 1, 1000);
	scene->cameras["first-person"]->setPosition(0.0, 0.0, zCoord - 10);

	//Assigned to key '2'
	scene->addCamera("front", true, 45, 1, 1000);
	scene->cameras["front"]->setPosition(0, 0, zCoord);
	scene->objects["diver"]->setPosition(0, yCoord - 2, -zCoord - 8);

	//Assigned to key '3'
	scene->addCamera("top", true, 45, 1, 1000);
	scene->cameras["top"]->setPosition(0, 0, zCoord);
	scene->cameras["top"]->setRotation(90, 0, 0);

	//Assigned to fish click
	scene->addCamera("details", true, 45, 1, 1000);
	scene->cameras["details"]->setPosition(0, -30, -20);

	scene->setFrameRate(30);

	//Default camera
	scene->setCurrentCamera("first-person");
	scene->objects["diver"]->setVisible(true);
}

void createLights() {
	LightManager *l = scene->getLightManager();
	//Light 0
	l->setDirectional(0, 0.0, 5.0, 0.0);
	//l->setPoint(0,0.0,10.0,0.0,100);
	//l->setDiffuse(0,1.0, 1.0, 1.0);

	//light 1
	l->setAmbient(1, 1, 1, 1);
	l->setEnabled(1, true);

	//Turn markers on
	l->setMarkers(false);

}

void idle() {
	scene->idle();
}

int main(int argc, char **argv) {
	srand(time(NULL));
	glutInit(&argc, argv);
	scene->begin(100, 100, 500, 500, "iQuarium", .2, .2, 1);
	glEnable(GL_BLEND);
	glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA);
	createLights();
	createScene();
	createCameras();
	glutDisplayFunc(render);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(specialKeyboard);
	glutMouseFunc(mouseClick);
	glutReshapeFunc(resize);
	glutIdleFunc(idle);
	glutMainLoop();
	return 0;
}

