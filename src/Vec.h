#ifndef VEC_H
#define VEC_H
#include "Classes.h"

class Vec
{
    public:
       GLfloat x;
       GLfloat y;
       GLfloat z;
       Vec(GLfloat x,GLfloat y,GLfloat z);
       void add(Vec *other);
       void setRandom(GLfloat min,GLfloat max);
       void setValues(GLfloat x, GLfloat y, GLfloat z);
};
#endif
