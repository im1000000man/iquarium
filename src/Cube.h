#ifndef CUBE_H
#define CUBE_H
#include "Classes.h"
#include "SceneObject.h"

class Cube : SceneObject
{
public:
	Cube(GLfloat size);
	void alphaCube(GLfloat size);
	void render();
	~Cube();

private:
	GLfloat size;
};

#endif
