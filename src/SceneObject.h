#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H
#include "Classes.h"
#include "Vec.h"

using namespace std;

class SceneObject {
public:
	SceneObject();
	void setPosition(const GLfloat x, const GLfloat y, const GLfloat z);
	Vec* getPosition();
	void setSpeed(const GLfloat x, const GLfloat y, const GLfloat z);
	Vec* getSpeed();
	void setColor(const GLfloat r, const GLfloat g, const GLfloat b);
	void setColor4(const GLfloat r, const GLfloat g, const GLfloat b,
			const GLfloat a);

	void setRotation(const GLfloat x, const GLfloat y, const GLfloat z);
	void setRotationSpeed(const GLfloat x, const GLfloat y, const GLfloat z);
	void setScale(const GLfloat x, const GLfloat y, const GLfloat z);
	void setScaleSpeed(const GLfloat x, const GLfloat y, const GLfloat z);

	void setRandomColor();
	void setRandomPosition(const GLfloat min, const GLfloat max);
	void setRandomSpeed(const GLfloat min, const GLfloat max);

	void setRandomScale(const GLfloat min, const GLfloat max);
	void setRandomScaleSpeed();
	void setRandomRotation(const GLfloat min, const GLfloat max);
	void setRandomRotationSpeed();
	void update();

	virtual void render() { };
	virtual ~SceneObject() {};
	virtual void action(Scene* scene, string objectName){};
	void deleteVectors();

	//Textures
	void setTextureId(int textureId);
	int getTextureId();
	void setTextGenMode(int textGenMode);
	int getTextGenMode();
	void setVisible(bool visible);
	bool isVisible();
	GLenum getWrapping();
	GLuint getHitName();
	void setWrapping(GLenum);
	void setHitName(GLuint hitName);

protected:
	Vec *position;
	Vec *speed;
	Vec *scale, *scaleSpeed, *rotation, *rotationSpeed;
	GLfloat r;
	GLfloat g;
	GLfloat b;
	GLfloat a;
	//Textures
	int textureId;
	GLenum textGenMode;
	int type;
	GLenum wrapping;
	bool visible;
	GLuint hitName;
};
#endif
