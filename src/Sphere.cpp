#include "Sphere.h"


Sphere::Sphere(GLdouble radius, GLint slices, GLint stacks) :SceneObject()
{
	this->radius = radius;
	this->slices = slices;
	this->stacks = stacks;
}

void Sphere::render()
{
	glColor3f(r, g, b);
	glutSolidSphere(this->radius,this->slices, this->stacks);
}

Sphere::~Sphere()
{
	deleteVectors();
}
