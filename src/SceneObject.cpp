#include "SceneObject.h"
#include <stdio.h>

SceneObject::SceneObject() {
	position = new Vec(0.0, 0.0, 0.0);
	speed = new Vec(0.0, 0.0, 0.0);
	scale = new Vec(1, 1, 1);
	scaleSpeed = new Vec(0.0, 0.0, 0.0);
	rotation = new Vec(0.0, 0.0, 0.0);
	rotationSpeed = new Vec(0.0, 0.0, 0.0);
	r = 1.0;
	g = 1.0;
	b = 1.0;
	a = 1.0;

	textureId = INVALID_TEXTURE;
	textGenMode = DEFAULT_MODE;
	wrapping=GL_REPEAT;
	visible=true;
}

void setVectorValues(Vec* vector, const GLfloat x, const GLfloat y,
		const GLfloat z) {
	vector->x = x;
	vector->y = y;
	vector->z = z;
}

Vec* setRandomVectorValues(GLfloat min, GLfloat max) {
	const GLfloat x = (max - min) * ((double) rand() / (double) RAND_MAX) + min;
	const GLfloat y = (max - min) * ((double) rand() / (double) RAND_MAX) + min;
	const GLfloat z = (max - min) * ((double) rand() / (double) RAND_MAX) + min;
	return new Vec(x, y, z);
}

void SceneObject::setPosition(const GLfloat x, const GLfloat y,
		const GLfloat z) {
	setVectorValues(this->position, x, y, z);
}

Vec* SceneObject::getPosition() {
	return this->position;
}

Vec* SceneObject::getSpeed() {
	return this->speed;
}

void SceneObject::setSpeed(const GLfloat x, const GLfloat y, const GLfloat z) {
	setVectorValues(this->speed, x, y, z);
}

void SceneObject::setColor(const GLfloat r, const GLfloat g, const GLfloat b) {
	this->setColor4(r, g, b, 1);
}
void SceneObject::setColor4(const GLfloat r, const GLfloat g, const GLfloat b,
		const GLfloat a) {
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

void SceneObject::update() {
	glPushMatrix();
	glTranslatef(position->x, position->y, position->z);
	glScalef(this->scale->x, this->scale->y, this->scale->z);
	glRotatef(this->rotation->x, 1.0, 0.0, 0.0);
	glRotatef(this->rotation->y, 0.0, 1.0, 0.0);
	glRotatef(this->rotation->z, 0.0, 0.0, 1.0);
	render();
	glPopMatrix();
	position->add(speed);
	rotation->add(rotationSpeed);
	scale->add(scaleSpeed);
}

void SceneObject::setRotation(const GLfloat x, const GLfloat y,
		const GLfloat z) {
	setVectorValues(this->rotation, x, y, z);
}

void SceneObject::setRotationSpeed(const GLfloat x, const GLfloat y,
		const GLfloat z) {
	setVectorValues(this->rotationSpeed, x, y, z);
}

void SceneObject::setScale(const GLfloat x, const GLfloat y, const GLfloat z) {
	setVectorValues(this->scale, x, y, z);
}

void SceneObject::setScaleSpeed(const GLfloat x, const GLfloat y,
		const GLfloat z) {
	setVectorValues(this->scaleSpeed, x, y, z);
}

void SceneObject::setRandomSpeed(const GLfloat min, const GLfloat max) {
	this->speed = setRandomVectorValues(min, max);
}

void SceneObject::setRandomScale(const GLfloat min, const GLfloat max) {
	this->scale = setRandomVectorValues(min, max);
}

void SceneObject::setRandomRotation(const GLfloat min, const GLfloat max) {
	this->rotation = setRandomVectorValues(min, max);
}

void SceneObject::setRandomColor() {
	//Set each one a random value between .001 and 1
	this->r = (rand() % 100) * 0.01;
	this->g = (rand() % 100) * 0.01;
	this->b = (rand() % 100) * 0.01;
}

void SceneObject::setRandomPosition(const GLfloat min, const GLfloat max) {
	this->position = setRandomVectorValues(min, max);
}

void SceneObject::deleteVectors() {
	delete position;
	delete speed;
	delete rotation;
	delete rotationSpeed;
	delete scale;
	delete scaleSpeed;
	//printf("deleteVectors()\n");
}

//Textures Section
void SceneObject::setTextureId(int textureId) {
	this->textureId = textureId;
}
int SceneObject::getTextureId() {
	return textureId;
}
void SceneObject::setTextGenMode(int textGenMode) {
	// Possible modes: GL_OBJECT_LINEAR GL_EYE_LINEAR GL_SPHERE_MAP
	this->textGenMode = textGenMode;
}
int SceneObject::getTextGenMode() {
	return textGenMode;
}

GLenum SceneObject::getWrapping() {
	return wrapping;
}

bool SceneObject::isVisible() {
	return visible;
}

void SceneObject::setVisible(bool visible) {
	this->visible = visible;
}

GLuint SceneObject::getHitName() {
	return hitName;
}

void SceneObject::setHitName(GLuint hitName) {
	this->hitName = hitName;
}

void SceneObject::setWrapping(GLenum wrapping){
	//Possible wrappings: GL_CLAMP, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_EDGE, GL_MIRRORED_REPEAT, or GL_REPEAT
	this->wrapping = wrapping;
}

