#include "DataDialog.h"

DataDialog::DataDialog(GLfloat width,GLfloat height,GLfloat alpha):SceneObject()
{
    this->width=width;
    this->height=height;
    this->alpha=alpha;
    this->textureId=INVALID_TEXTURE;
    this->wrapping=GL_CLAMP_TO_BORDER;
}

DataDialog::DataDialog(GLfloat width,GLfloat height,GLfloat alpha,GLenum wrapping):SceneObject()
{
    this->width=width;
    this->height=height;
    this->alpha=alpha;
    this->textureId=INVALID_TEXTURE;
    this->wrapping=wrapping;
}

DataDialog::~DataDialog()
{
    deleteVectors();
}


void DataDialog::render()
{
	//Dialog Text Message
	glRasterPos3f(this->position->x - this->width/2, this->position->y, this->position->z + .2);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glutBitmapString(GLUT_BITMAP_HELVETICA_12, (const unsigned char*) ("Acanthocybium Wahoo "));
	glRasterPos3f(this->position->x - this->width/2, this->position->y -.5, this->position->z + .2);
	glutBitmapString(GLUT_BITMAP_HELVETICA_12, (const unsigned char*) ("Range: Atlantic, Indian and Pacific: in tropical and subtropical waters,"));
	glRasterPos3f(this->position->x - this->width/2, this->position->y -1, this->position->z + .2);
	glutBitmapString(GLUT_BITMAP_HELVETICA_12, (const unsigned char*) ("including the Caribbean and Mediterranean seas."));

	//
 /*   glDisable(GL_LIGHTING);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1.0,1.0,1.0,alpha);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GEQUAL,alpha);
    
    GLfloat x = 0.0 - (width/ 2.0);
    GLfloat y = 0.0 - (height / 2.0);


    glBegin(GL_QUADS);
    glTexCoord2f(1.0f, 0.0f); glVertex3f(x + width,y,0.0);
    glTexCoord2f(1.0f, 1.0f); glVertex3f(x + width, y + height, 0.0);
    glTexCoord2f(0.0f, 1.0f); glVertex3f(x,y + height,0.0);
    glTexCoord2f(0.0f, 0.0f); glVertex3f(x,y,0.0);
    glEnd();
    
    glDisable(GL_BLEND);
    glDisable(GL_ALPHA_TEST);
    glEnable(GL_LIGHTING);
    */
}

