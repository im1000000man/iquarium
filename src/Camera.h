#ifndef CAMERA_H
#define CAMERA_H
#include "SceneObject.h"
#include "Classes.h"

class Camera :SceneObject
{
public:
	Camera(bool perspective, GLfloat fovy, GLfloat nearPlane, GLfloat farPlane);
	void setPosition(GLfloat x, GLfloat y, GLfloat z);
	void setSpeed(GLfloat x, GLfloat y, GLfloat z);
	void setRotation(GLfloat x, GLfloat y, GLfloat z);
	void setRotationSpeed(GLfloat x, GLfloat y, GLfloat z);
    void setForwardSpeed();
    void setBackwardSpeed();
	bool getPerspective();
	GLfloat getFovy();
	GLfloat getNear();
	GLfloat getFar();
	GLfloat getLeft();
	GLfloat getRight();
	GLfloat getTop();
	GLfloat getBottom();
	Vec* getPosition();
	Vec* getRotation();
	~Camera();

private:
	bool perspective;
	GLfloat fovy, nearPlane, farPlane;
};
#endif
