#include "Scene.h"

string diverName = "diver";

Scene::Scene() {
	lightManager = new LightManager();
	textureManager = new TextureManager();
	lastHitname = 0;
	limits = new Vec(100, 100, 100);
}

void Scene::begin(int left, int top, int width, int height, string title,
		GLfloat r, GLfloat g, GLfloat b) {
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(left, top);
	glutInitWindowSize(width, height);
	glutCreateWindow(title.c_str());
	glClearColor(r, g, b, 1.0);
	//Default frame rate: 30 fps 
	setFrameRate(30);
	currentTime = glutGet(GLUT_ELAPSED_TIME);
	//Create a default camera and set it as current 
	addCamera("default", true, 45, 1.0, 1000.0);
	cameras["default"]->setPosition(0.0, 0.0, -5.0);
	currentCamera = cameras["default"];

}

LightManager* Scene::getLightManager() {
	return this->lightManager;
}

void Scene::render() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	lightManager->lightScene();
	glLoadIdentity();
	map<string, SceneObject *>::const_iterator iter;
	glPushName(0);
	for (iter = objects.begin(); iter != objects.end(); ++iter) {
		SceneObject *o = (SceneObject *) iter->second;
		if (o->isVisible()) {
			glLoadName(o->getHitName());
			if (o->getTextureId() != INVALID_TEXTURE) {
				textureManager->activateTexture(o->getTextureId(),
						o->getWrapping());
				if (o->getTextGenMode() > DEFAULT_MODE) {
					textureManager->textureCoordsGen(o->getTextGenMode());
				}
			}
			//Change direction if it is a fish and its outside the fishtank
			string objectName = (string) iter->first;
			SceneObject *o = (SceneObject *) iter->second;

			this->collisions(objectName, o);

			o->update();
			textureManager->resetTexturing();
		}
	}
	glutSwapBuffers();
}

void Scene::collisions(string objectName, SceneObject *o){

	if (strncmp(objectName.c_str(), "fish_", 5) == 0){
		Vec* fishPosition = o->getPosition();
		if (this->collision(fishPosition->x, limits->x)){
			fishPosition->x -= o->getSpeed()->x;
			o->getSpeed()->x *= -1;
		}
		if (this->collision(fishPosition->y, limits->y)){
			fishPosition->y -= o->getSpeed()->y;
			o->getSpeed()->y *= -1;
		}
		if (this->collision(fishPosition->z, limits->z)){
			fishPosition->z -= o->getSpeed()->z;
			o->getSpeed()->z *= -1;
		}

	}
}

bool Scene::collision(GLfloat model_coordinate, GLfloat limit_coordinate){
	return abs(model_coordinate) > abs(limit_coordinate);
}

void Scene::resize(int currentWidth, int currentHeight) {
	windowWidth = currentWidth;
	windowHeight = currentHeight;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (currentCamera->getPerspective()) {
		GLfloat aspect = (GLfloat) windowWidth / (GLfloat) windowHeight;
		gluPerspective(currentCamera->getFovy(), aspect,
				currentCamera->getNear(), currentCamera->getFar());
	} else {
		glOrtho(currentCamera->getLeft(), currentCamera->getRight(),
				currentCamera->getBottom(), currentCamera->getTop(),
				currentCamera->getNear(), currentCamera->getFar());
	}
	Vec *position = currentCamera->getPosition();
	Vec *rotation = currentCamera->getRotation();

	glTranslatef(position->x, position->y, position->z);
	glRotatef(rotation->x, 1.0, 0.0, 0.0);
	glRotatef(rotation->y, 0.0, 1.0, 0.0);
	glRotatef(rotation->z, 0.0, 0.0, 1.0);
	glViewport(0, 0, windowWidth, windowHeight);
	glMatrixMode(GL_MODELVIEW);
}

void Scene::keyboard(unsigned char key, int x, int y) {

	switch (key) {
	case ESC:
		end();
		break;
	case '1':
		//Diver view: FPS
		this->setCurrentCamera("first-person");
		objects[diverName]->setVisible(true);
		this->resize(windowWidth, windowHeight);
		break;
	case '2':
		//Front view
		this->setCurrentCamera("front");
		objects[diverName]->setVisible(false);
		this->resize(windowWidth, windowHeight);
		break;

	case '3':
		//Top view
		this->setCurrentCamera("top");
		this->resize(windowWidth, windowHeight);
		break;
	case '4':
		//Top view
		this->setCurrentCamera("details");
		this->resize(windowWidth, windowHeight);
		break;
	default:
		printf("User pressed an unknown key: %c \n", key);
		break;
	}
}

void Scene::specialKeyboard(int key, int x, int y) {
	GLfloat delta = 0.5;
	Vec* currentCameraPosition = currentCamera->getPosition();
	Vec* currentDiverPosition;
	cout << currentCameraString << endl;
	if(currentCameraString == "first-person")
		currentDiverPosition = objects[diverName]->getPosition();
	else
		currentDiverPosition = new Vec(0,0,0);
	//cout << "camPos: " << currentCameraPosition->x << ", " <<currentCameraPosition->y << ", " << currentCameraPosition->z << endl;
	//cout << "divPos: " << currentDiverPosition->x << ", " <<currentDiverPosition->y << ", " << currentDiverPosition->z << endl;

	switch (key) {
	case GLUT_KEY_UP:
		currentCameraPosition->y += delta;
		currentDiverPosition->y -= delta;
		break;

	case GLUT_KEY_DOWN:
		currentCameraPosition->y -= delta;
		currentDiverPosition->y += delta;
		break;

	case GLUT_KEY_PAGE_UP:
		currentCameraPosition->z += delta;
		currentDiverPosition->z -= delta;
		break;

	case GLUT_KEY_PAGE_DOWN:
		currentCameraPosition->z -= delta;
		currentDiverPosition->z += delta;
		break;

	case GLUT_KEY_LEFT:
		currentCameraPosition->x -= delta;
		currentDiverPosition->x += delta;
		break;

	case GLUT_KEY_RIGHT:
		currentCameraPosition->x += delta;
		currentDiverPosition->x -= delta;
		break;
	}
	this->resize(windowWidth, windowHeight);
}

void Scene::mouseClick(int button, int state, int x, int y) {
	bool leftClick;
	bool rightClick;
	switch (button) {
	case GLUT_LEFT_BUTTON:
		leftClick = (state == GLUT_DOWN);
		break;

	case GLUT_RIGHT_BUTTON:
		rightClick = (state == GLUT_DOWN);
		break;
	}
	if (leftClick) {
		startPicking(x, y);
		this->render();
		stopPicking();
	}
}

void Scene::end() {
	map<string, SceneObject *>::const_iterator iter;
	for (iter = objects.begin(); iter != objects.end(); ++iter) {
		SceneObject *o = (SceneObject *) iter->second;
		printf("object:%s cleanup:", ((string) iter->first).c_str());
		delete o;
	}
	map<string, Camera *>::const_iterator itercam;
	for (itercam = cameras.begin(); itercam != cameras.end(); ++itercam) {
		SceneObject *c = (SceneObject *) itercam->second;
		printf("camera:%s cleanup:", ((string) itercam->first).c_str());
		delete c;
	}
	cameras.clear();
	objects.clear();
	exit(0);
}

void Scene::add(SceneObject *newObject,string name)
{
    objects[name]=newObject;
    objects[name]->setHitName(lastHitname);
    lastHitname++;
}

void Scene::addCamera(string name, bool perspective, GLfloat fovy,
		GLfloat nearPlane, GLfloat farPlane) {
	cameras[name] = new Camera(perspective, fovy, nearPlane, farPlane);
}

void Scene::setCurrentCamera(string name) {
	currentCamera = cameras[name];
	currentCameraString = name;
}

Camera* Scene::getCurrentCamera(){
	return this->currentCamera;
}

void Scene::idle() {
	GLint timeDifference = glutGet(GLUT_ELAPSED_TIME) - currentTime;

	if (timeDifference > frameTime) {
		currentFrame++;
		currentTime = glutGet(GLUT_ELAPSED_TIME);
		glutPostRedisplay();
	}

	if (currentTime % 2000 <= 1) {
		//printf("currT: %d , tDiff: %d \n", currentTime, timeDifference);
	}
}

void Scene::setFrameRate(int frameRate) {
	this->frameRate = frameRate;
	this->frameTime = 1000 / frameRate;
}

TextureManager *Scene::getTextureManager() {
	return textureManager;
}
void Scene::setObjectTexture(string objectName, string textureName) {
	int textureId = textureManager->getTextureId(textureName);
	if (textureId != INVALID_TEXTURE)
		objects[objectName]->setTextureId(textureId);
}

void Scene::startPicking(int cursorX, int cursorY) {

	GLint viewport[4];
	glSelectBuffer(PICKBUFSIZE, selectBuf);
	glRenderMode(GL_SELECT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glGetIntegerv(GL_VIEWPORT, viewport);
	gluPickMatrix(cursorX, viewport[3] - cursorY, 5, 5, viewport);

	if (currentCamera->getPerspective()) {
		GLfloat aspect = (GLfloat) windowWidth / (GLfloat) windowHeight;
		gluPerspective(currentCamera->getFovy(), aspect,
				currentCamera->getNear(), currentCamera->getFar());
	} else {
		glOrtho(currentCamera->getLeft(), currentCamera->getRight(),
				currentCamera->getBottom(), currentCamera->getTop(),
				currentCamera->getNear(), currentCamera->getFar());
	}
	Vec *position = currentCamera->getPosition();
	Vec *rotation = currentCamera->getRotation();

	glTranslatef(position->x, position->y, position->z);
	glRotatef(rotation->x, 1.0, 0.0, 0.0);
	glRotatef(rotation->y, 0.0, 1.0, 0.0);
	glRotatef(rotation->z, 0.0, 0.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glInitNames();
}

void Scene::stopPicking() {

	int hits;

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glFlush();

	hits = glRenderMode(GL_RENDER);
	if (hits != 0)
		processHits(hits);
}

void Scene::processHits(GLint hits) {
	GLuint i, j, index, nitems, zmin, zmax, selection;

	for (i = 0, index = 0; i < hits; i++) {
		nitems = selectBuf[index++];
		zmin = selectBuf[index++];
		zmax = selectBuf[index++];

		for (j = 0; j < nitems; j++) {
			selection = selectBuf[index++];
			// Se busca el objeto de la escena con el hitName seleccionado
			map<string, SceneObject *>::const_iterator iter;
			for (iter = this->objects.begin(); iter != this->objects.end();
					++iter) {
				SceneObject *o = (SceneObject *) iter->second;
				string objectName = (string) iter->first;
				if (o->getHitName() == selection) {
					actions(objectName);
				}
			}
		}
	}
}

void Scene::actions(string objectName) {
	objects[objectName]->action(this, objectName);
}

void Scene::setLimits(const GLfloat x, const GLfloat y, const GLfloat z){
	this->limits->setValues(x, y, z);
}
Vec* Scene::getLimits(){
	return this->limits;
}

void Scene::resize(){
	this->resize(windowWidth, windowHeight);
}
