#pragma once
#include "Classes.h"
#include "SceneObject.h"

class Quad : SceneObject
{
public:
    Quad(GLfloat width,GLfloat height,GLfloat alpha);
    Quad(GLfloat width,GLfloat height,GLfloat alpha,GLenum wrapping);
    ~Quad();
    void render();
    
private:
    GLfloat width;
    GLfloat height;
    GLfloat alpha;
};

