################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Camera.cpp \
../src/Cube.cpp \
../src/DataDialog.cpp \
../src/Light.cpp \
../src/LightManager.cpp \
../src/Model.cpp \
../src/Quad.cpp \
../src/Scene.cpp \
../src/SceneObject.cpp \
../src/Sphere.cpp \
../src/Teapot.cpp \
../src/TextureManager.cpp \
../src/Vec.cpp \
../src/glm.cpp \
../src/iQuarium.cpp 

OBJS += \
./src/Camera.o \
./src/Cube.o \
./src/DataDialog.o \
./src/Light.o \
./src/LightManager.o \
./src/Model.o \
./src/Quad.o \
./src/Scene.o \
./src/SceneObject.o \
./src/Sphere.o \
./src/Teapot.o \
./src/TextureManager.o \
./src/Vec.o \
./src/glm.o \
./src/iQuarium.o 

CPP_DEPS += \
./src/Camera.d \
./src/Cube.d \
./src/DataDialog.d \
./src/Light.d \
./src/LightManager.d \
./src/Model.d \
./src/Quad.d \
./src/Scene.d \
./src/SceneObject.d \
./src/Sphere.d \
./src/Teapot.d \
./src/TextureManager.d \
./src/Vec.d \
./src/glm.d \
./src/iQuarium.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include/GL/ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


