################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Camera.cpp \
../src/Cube.cpp \
../src/Lab007.cpp \
../src/Model.cpp \
../src/Scene.cpp \
../src/SceneObject.cpp \
../src/Sphere.cpp \
../src/Teapot.cpp \
../src/Vec.cpp \
../src/glm.cpp \
../src/lab06.cpp 

OBJS += \
./src/Camera.o \
./src/Cube.o \
./src/Lab007.o \
./src/Model.o \
./src/Scene.o \
./src/SceneObject.o \
./src/Sphere.o \
./src/Teapot.o \
./src/Vec.o \
./src/glm.o \
./src/lab06.o 

CPP_DEPS += \
./src/Camera.d \
./src/Cube.d \
./src/Lab007.d \
./src/Model.d \
./src/Scene.d \
./src/SceneObject.d \
./src/Sphere.d \
./src/Teapot.d \
./src/Vec.d \
./src/glm.d \
./src/lab06.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


